# 3088 PiHat Project

The piano microPiHat

The description of the microPi Hat:
The proposed microHAT is the piano circuit. This microHAT is based on decision making and signals. The aim is to produce a signal from the power a DC power source and output the signal to the speaker(sound). A digital piano is designed using buttons(keys), where one key/button’s frequency will be changed for different sounds.
The keys/buttons will be configured so that different keys can be used simultaneously. The piano circuit will also include the volume knob so that the user can be able to choose their preferred level of sound. LEDs will be used as an indication of which key/button has been pressed.
This piano can be used at indoors events, also in musical schools. A scenario where this piano can be useful, is when used in the musical schools where LEDs will assist visual students to remember the keys.


# More content about the Project

The cost of the material should not exceed the budget of R150.00
The microPi Hat will have keys/buttons which will be used for different sounds of the paino. LEDs on the Hat will determine different keys with different sounds.
